<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>


    <?= form_open('editar/editarhotel/' . $grupo->id) ?>
    <div class="form-group row">
        <?= form_label('Código:', 'codigo', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('codigo', $grupo->descripcion, ['class' => 'form_control', 'id' => 'codigo']) ?>
        </div>
    </div>
    <div class="form-group row">
        <?= form_label('Nombre:', 'nombre', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('nombre', $grupo->nombre, ['class' => 'form_control', 'id' => 'nombre']) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-10">
            <?= form_submit('boton', 'Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= form_close() ?>
<?= $this->endSection() ?>

