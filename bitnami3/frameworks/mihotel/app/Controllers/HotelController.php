<?php

namespace App\Controllers;
use App\Models\Hotelmodel;

class HotelController extends BaseController
{
    public function muestrahotel()
    {
        $data['title'] = 'Listado de Hoteles';
        $grupoModel = new HotelModel();
        $data['grupo'] = $grupoModel->findAll();
        return view('grupo/listadohoteles', $data);
}
}
