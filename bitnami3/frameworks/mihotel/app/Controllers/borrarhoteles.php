<?php


namespace App\Controllers;
use App\Models\Hotelmodel;  


class borrarhoteles extends BaseController {
    
    public function grupo($data){
        $grupoModel = new Hotelmodel();
        $data['grupo'] = $grupoModel->where('codigo', $data)->delete();
        return  redirect()->to('grupo/listadohoteles');
    }
}