<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\Controller;
use App\Models\PostModel;


/**
 * Description of PostController
 *
 * @author jose
 */
class PostController extends BaseController{
   
    protected $post;


    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        // Preload any models, libraries, etc, here.

        $this->post = new PostModel();
    }
    
    
    public function index(){
        $data['title'] = 'Blog de Seguidad Informática de ASIR';
        $data['ionAuth'] = $this->ionAuth;
        $data['posts'] = $this->post->select('post.*')
                ->select('COUNT(*) as num_comentarios')
                ->select("CONCAT(users.first_name,' ',users.last_name) as autor")
                ->join('users','users.id = post.usuario','LEFT')
                ->join('comentarios','comentarios.post=post.id','LEFT')
                ->groupBy('post.id')
                ->findAll();
        /*echo '<pre>';
        print_r($data);
        echo '</pre>';*/
        return view('post/principal',$data);
    }
      public function fullscreen($id) {
        $commentModel = new \App\Models\CommentModel();
        $data['title'] = 'Blog de Seguidad Informática de ASIR';
        $data['ionAuth'] = $this->ionAuth;
       $data['comentario'] = $commentModel->select('comentarios.comentario','comentarios.creado')
                ->join('post','post.id = comentarios.post','LEFT')
                ->where('comentarios.post',$id)
                ->findAll();
       $data['posts']  = $this->post->select('post.*')
                ->select('COUNT(*) as num_comentarios')
                ->select("CONCAT(users.first_name,' ',users.last_name) as autor")
                ->join('users','users.id = post.usuario','LEFT')
                ->join('comentarios','comentarios.post=post.id','LEFT')
                ->groupBy('post.id')
                ->findAll($id);
      
           return view('post/comentarios', $data);
       
    }
}


