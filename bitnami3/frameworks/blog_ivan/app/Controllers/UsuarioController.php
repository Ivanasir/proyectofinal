<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
namespace App\Controllers;
use App\Models\UsuarioModel; //decimos donde está

use Config\Services;

/**
 * Description of AlumnoController
 *
 * @author jose
 */
class UsuarioController extends BaseController {


 public function inserta(){
        $data['title'] = 'Formulario Usuarios';
        helper('form');
        if (strtolower($this->request->getMethod()) !== 'post') { //la primera vez
           return view('alumno/formusu', $data); 
        } else {
            $usuario_nuevo = [
            'id' => $this->request->getPost('id'),
            'titulo' => $this->request->getPost('titulo'),
            'contenido' => $this->request->getPost('contenido'),
            'creado_at' => $this->request->getPost('creado_at'),
            'usuario' => $this->request->getPost('usuario'),
        ];
         $usuario_nuevo = new UsuarioModel();
            if ($UsuarioModel->insert($usuario_nuevo) === false){
               $data['errores'] = $UsuarioModel->errors(); 
               return view('auth/formusuario', $data);  
            }
        }
 }
}
