<?php

namespace App\Controllers;

use App\Models\PecesDulceModel;
use App\Models\PecesSaladaModel;

class PecesController extends BaseController {

    public function home() {
        return view('home');
    }

    public function adulce() {
        $adulceModel = new PecesDulceModel();
        $data['adulce'] = $adulceModel->findAll();
        return view('category', $data);
    }

    public function asalada() {
        $asaladaModel = new PecesSaladaModel();
        $data['asalada'] = $asaladaModel->findAll();
        return view('category2', $data);
    }

    public function productosASalada($idProd) {
        $asaladaModel = new PecesSaladaModel();
        $data['producto'] = $asaladaModel->find($idProd);
        return view('product', $data);
    }

    public function productosADulce($idProducto) {
        $adulceModel = new PecesDulceModel();
        $data['producto'] = $adulceModel->find($idProducto);
        return view('productDulce', $data);
    }

    public function registro() {
        return view('register');
    }

    public function colvidar() {
        return view('forgotten-password');
    }

    public function cuenta() {
        return view('login');
    }

    public function compra() {
        return view('checkout');
    }

    public function metodoenvio() {
        return view('checkout-shipping');
    }

    public function formapago() {
        return view('checkout-payment');
    }

    public function anyadirProd() {
        // Iniciar la sesión
        session()->start();

        // COGER DATOS DE FORMULARIO
        $pedidos = session()->get('inforPedido', array());

        $pedidos[] = $data['nuevoPedido '] = array(
            'id' => $this->request->getPost('id'),
            'imagen' => $this->request->getPost('imagen'),
            'nombre' => $this->request->getPost('nombre'),
            'precio' => $this->request->getPost('precio')
        );
        
        session()->set('inforPedido', $pedidos);
        
        // Redirigir al usuario a la página anterior
        return view('home');
    }
    
    public function carrito() {
        $data['carrito'] = session()->get('carrito', 0);
        $data['nuevoPedido'] = session()->get('inforPedido');

        if (empty($data['nuevoPedido'] = session()->get('inforPedido'))) {
            //return view('empty_cart.php');
            echo "<script>
                        alert('No hay ningún producto en el carrito. Añade uno antes de continuar.');
                        window.history.back()
                   </script>";

            //return redirect()->back();
        } else {
            return view('cart', $data);
        }
    }
    
    public function eliminar($idProd) {
        $cartNum = session()->get('carrito', 0);
        $cartNum--;
        session()->set('carrito', $cartNum);
        $carrito = session()->get('inforPedido');
        foreach ($carrito as $id => $producto) {
            if ($producto['id'] == $idProd) {
                unset($carrito[$id]);
                break;
            }
        }
        session()->set('inforPedido', $carrito);
        return redirect()->to('https://pro.ausiasmarch.es:50878/home');
    }

}
