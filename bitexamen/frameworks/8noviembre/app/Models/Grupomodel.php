<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Grupomodel
 *
 * @author a024463185v
 */

namespace app\Models;
use CodeIgniter\Model;


class Grupomodel extends Model {
    
    protected $table = 'pau';
    protected $primarykey = 'id';
    protected $returnType = 'object';        
}
