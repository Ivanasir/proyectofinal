<?php

namespace App\Controllers;



class Grupo extends BaseController
{
    public function muestraCiclo()
    {
        $data['title'] = 'Listado Ciclos';
        $grupoModel = new \App\Models\GrupoModel();
        $data['grupos'] = $grupoModel->findAll();
        return view('grupo/lista',$data);
    }
}
